USE [Customer]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 12/5/2020 4:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FIRST_NAME] [varchar](50) NOT NULL,
	[LAST_NAME] [varchar](50) NOT NULL,
	[AGE] [int] NOT NULL,
	[ADDRESS_CITY] [varchar](50) NOT NULL,
	[ADDRESS_STREET] [varchar](50) NOT NULL,
	[PH_NUMBER] [varchar](50) NOT NULL,
 CONSTRAINT [PK__Customer__3214EC274E77F31E] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([ID], [FIRST_NAME], [LAST_NAME], [AGE], [ADDRESS_CITY], [ADDRESS_STREET], [PH_NUMBER]) VALUES (17, N'MARTIN', N'ALPARAN', 29, N'SAN DEIGO', N'LA MESA', N'5421369870')
INSERT [dbo].[Customer] ([ID], [FIRST_NAME], [LAST_NAME], [AGE], [ADDRESS_CITY], [ADDRESS_STREET], [PH_NUMBER]) VALUES (20, N'MARTIN', N'ALPARAN', 29, N'SAN DEIGO', N'LA MESA', N'200314500')
INSERT [dbo].[Customer] ([ID], [FIRST_NAME], [LAST_NAME], [AGE], [ADDRESS_CITY], [ADDRESS_STREET], [PH_NUMBER]) VALUES (22, N'FAYDEE', N'CHOBOCH', 18, N'SAN LOUIS', N'LA NINA', N'960053201')
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Customer__60EA99B167517B7A]    Script Date: 12/5/2020 4:24:53 PM ******/
ALTER TABLE [dbo].[Customer] ADD  CONSTRAINT [UQ__Customer__60EA99B167517B7A] UNIQUE NONCLUSTERED 
(
	[PH_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
