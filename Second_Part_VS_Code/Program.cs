﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson29DAO_hw
{
    class Program
    {
        static void PrintAllRecord(CustomerDAO customerDAO)
        {
            List<Customer> customer = customerDAO.GetCustomers();
            Console.WriteLine("GetAllCustomers");
            customer.ForEach(c => Console.WriteLine(c));
            Console.WriteLine();
            Console.WriteLine(customer.Capacity);
        }
        static void Main(string[] args)
        {
            string conn_string = "Data Source =.; Initial Catalog = Customer; Integrated Security = True";

            CustomerDAO customerDAO = new CustomerDAO(conn_string);

            //PrintAllRecord(customerDAO);
            
            Customer van = new Customer
            {

                FirstName = "VAN",
                LastName = "HERSTON",
                Age = 29,
                Address_City = "LOS ANGELS",
                Address_Street = "LA MESA",
                PhoneNumebr = "0532641897"
            };

            Customer martin = new Customer
            {

                FirstName = "MARTIN",
                LastName = "ALPARAN",
                Age = 29,
                Address_City = "SAN DEIGO",
                Address_Street = "LA MESA",
                PhoneNumebr = "200314500"
            };

            Customer faydee = new Customer
            {

                FirstName = "FAYDEE",
                LastName = "CHOBOCH",
                Age = 18,
                Address_City = "SAN LOUIS",
                Address_Street = "LA NINA",
                PhoneNumebr = "960053201"
            };
            //customerDAO.AddCustomer(van);
            //customerDAO.AddCustomer(martin);
            customerDAO.AddCustomer(faydee);
            //PrintAllRecord(customerDAO);

            customerDAO.GetCustomerByID(2);
            //customerDAO.RemoveAllCustomers();

            //customerDAO.DeleteCustomer(16);
            PrintAllRecord(customerDAO);

            Console.WriteLine(customerDAO.GetCustomerByPhoneNumberLINQ("200314500"));






        }
    }
}