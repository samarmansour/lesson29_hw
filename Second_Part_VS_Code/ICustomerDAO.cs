﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson29DAO_hw
{
    interface ICustomerDAO
    {
        List<Customer> GetCustomers();
        Customer GetCustomerByID(int id);
        void AddCustomer(Customer customer);
        void UpdateCustomer(int id, Customer customer);
        void DeleteCustomer(int id);
        List<Customer> GetCustomersLivingInCity(Customer city);
        List<Customer> GetCustomersBetweenAges(int minAge, int maxAge);
        Customer GetCustomerByPhoneNumber(string phone);
        void RemoveAllCustomers();



    }
}
