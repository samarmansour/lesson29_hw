﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson29DAO_hw
{
    class CustomerDAO : ICustomerDAO
    {
        private string m_conn_string;
        public CustomerDAO(string conn_string)
        {
            m_conn_string = conn_string;
        }
        private int ExecuteNonQuery(string query)
        {
            int result;
            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }

        private Customer ReadAllRecords(string query)
        {
            Customer result = null;
            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        result = new Customer
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            FirstName = reader["FIRST_NAME"].ToString(),
                            LastName = reader["LAST_NAME"].ToString(),
                            Age = Convert.ToInt32(reader["AGE"]),
                            Address_City = reader["ADDRESS_CITY"].ToString(),
                            Address_Street = reader["ADDRESS_STREET"].ToString(),
                            PhoneNumebr = reader["PH_NUMBER"].ToString()
                        };
                    }
                }
            }
            return result;
        }

        private List<Customer> ReadAllListRecords(string query)
        {
            List<Customer> result = new List<Customer>();
            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_conn_string))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read() == true)
                    {
                        Customer c = new Customer
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            FirstName = reader["FIRST_NAME"].ToString(),
                            LastName = reader["LAST_NAME"].ToString(),
                            Age = Convert.ToInt32(reader["AGE"]),
                            Address_City = reader["ADDRESS_CITY"].ToString(),
                            Address_Street = reader["ADDRESS_STREET"].ToString(),
                            PhoneNumebr = reader["PH_NUMBER"].ToString()
                        };
                        result.Add(c);
                    }
                }
            }
            return result;
        }

        public void CreateTable()
        {
            using (SqlCommand cmd = new SqlCommand($"CREATE TABLE 'CUSTOMER' ('ID'    INT PRIMARY KEY," +
                    "'FIRST_NAME'    varchar(50) NOT NULL," +
                    "'LAST_NAME' varchar(50) NOT NULL," +
                    "'AGE'   INT NOT NULL," +
                    "'ADDRESS_CITY'  varchar(50)," +
                    "'ADDRESS_STREET'    varchar(50)," +
                    "'PH_NUMBER' varchar(50) UNIQUE)")) 
            {
                cmd.ExecuteNonQuery();
            }
        }
        public void AddCustomer(Customer customer)
        {
            ExecuteNonQuery($"INSERT INTO Customer(FIRST_NAME, LAST_NAME, AGE, ADDRESS_CITY, ADDRESS_STREET,PH_NUMBER)" +
                            $"VALUES('{customer.FirstName}','{customer.LastName}', {customer.Age}," +
                            $"'{customer.Address_City}','{customer.Address_Street}','{customer.PhoneNumebr}')");
        }

        public void DeleteCustomer(int id)
        {
            ExecuteNonQuery($"DELETE FROM Customer WHERE ID = {id}");
        }

        public Customer GetCustomerByID(int id)
        {
            return ReadAllRecords($"SELECT * FROM Customer WHERE ID = {id}");
        }

        public Customer GetCustomerByPhoneNumber(string phone)
        {
            return ReadAllRecords($"SELECT * FROM Customer WHERE PH_NUMBER = {phone}");
        }

        public List<Customer> GetCustomers()
        {
            return ReadAllListRecords("SELECT * FROM Customer");
        }

        public List<Customer> GetCustomersBetweenAges(int minAge, int maxAge)
        {
            return ReadAllListRecords($"SELECT * FROM Customer WHERE AGE > {minAge} AND AGE < {maxAge}");
        }

        public List<Customer> GetCustomersLivingInCity(Customer city)
        {
            return ReadAllListRecords($"SELECT * FROM Customer WHERE CITY={city}");
        }

        public void RemoveAllCustomers()
        {
            ExecuteNonQuery("DELETE FROM Customer");
        }

        public void UpdateCustomer(int id, Customer customer)
        {
            ExecuteNonQuery($"UPDATE FROM CUSTOMER SET FIRST_NAME={customer.FirstName}, LAST_NAME={customer.LastName}," +
                            $"AGE = {customer.Age}, ADDRESS_CITY={customer.Address_City}, ADDRESS_STREET={customer.Address_Street}," +
                            $"PH_NUMBER={customer.PhoneNumebr} WHERE ID ={id}");
        }

        //===============================Challenge LINQ====================================

        public Customer GetCustomerByPhoneNumberLINQ(string phone)
        {
            List<Customer> customers = GetCustomers();
            var result =  customers.Where(c => c.PhoneNumebr == phone);
            return result.FirstOrDefault();
            /* Method way == SQL sentax
             var result = from c in customers where c.PhoneNumebr = phone select c;
             */
        }

        public List<Customer> GetCustomersBetweenAgesLINQ(int minAge, int maxAge)
        {
            List<Customer> customers = GetCustomers();
            var result = customers.Where(c => c.Age > minAge && c.Age < maxAge).FirstOrDefault();
            return result;
        }

        public List<Customer> GetCustomersLivingInCityLINQ(string city)
        {
            List<Customer> customers = GetCustomers();
            var result = customers.Where(c => c.Address_City == city);
            return result.FirstOrDefault();
        }


        public Customer GetCustomerByIDLINQ(int id)
        {
            List<Customer> customers = GetCustomers();
            var result = customers.Where(c => c.ID == id);
            return result.FirstOrDefault();
        }
    }
}
